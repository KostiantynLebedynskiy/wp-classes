<?php
namespace FX;
class ThemeOption {
	public function __construct() {
		add_action( 'admin_init', array($this,'theme_options_init'));
		add_action( 'admin_menu', array($this,'theme_options_add_page'));
		add_action( 'admin_enqueue_scripts', array($this,'prfx_image_enqueue'));
	}
	public static function theme_options_init(){
		register_setting( 'flamix_options', 'flamix_theme_options');
	}
	public static function theme_options_add_page() {
		add_theme_page( __( 'Настройка темы', 'WP-flamix' ), __( 'Настройка темы', 'WP-flamix' ), 'edit_theme_options', 'theme_options', array($this,'theme_options_do_page') );
	}
	public static function theme_options_do_page() { global $select_options; if ( ! isset( $_REQUEST['settings-updated'] ) ) $_REQUEST['settings-updated'] = false;
?>
	 <div class="wrapper-content">
		<div class="wrap">
		<?php screen_icon(); echo "<h2>". __( 'Настройка темы', 'WP-flamix' ) . "</h2>"; ?>
		<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
		<div id="message" class="updated">
		<p><strong><?php _e( 'Настройки были сохранены', 'WP-flamix' ); ?></strong></p>
		</div>
		<?php endif; ?>
		</div>
		 
		<form method="post" action="options.php">
		<?php settings_fields( 'flamix_options' ); ?>
		<?php $options = get_option( 'flamix_theme_options' ); ?>
		<div class="lang">
			<ul>
				<li class="active"><a href="#"> RU </a></li>
				<li><a href="#"> KZ </a></li>
			</ul>
		</div>
		<div class="tab-lang active ru">
			<div class="wrapp-content active">
				<div class="left-block">
					<ul>
						<li class="active">Контакты</li> 
						<li >Социальные сети</li>
					</ul>
				</div>
				<div class="right-block">
					
					<div class="tab-content active">
						<ul class="b-contact">
							<li>
								<label>
					            	<span>Телефон для справок:</span>
					            	<input type="tel" name="flamix_theme_options[phone]" value="<?php echo $options['phone'];?>" />
					            </label>
							</li>
							<li>
								<label>
					            	<span>Караоке-системы:</span>
					                <input name="flamix_theme_options[karaoke_tel]" value="<?php echo $options['karaoke_tel'];?>" />
					            </label>
							</li>
							<li>
								<label>
					            	<span>Звуковое оборуд.:</span>
					                <input name="flamix_theme_options[sound_tel]" value="<?php echo $options['sound_tel'];?>" />
					            </label>
							</li>
							<li>
								<label>
					            	<span>Световое оборуд.:</span>
					                <input name="flamix_theme_options[light_tel]" value="<?php echo $options['light_tel'];?>" />
					            </label>
							</li>
							<li>
								<label>
					            	<span>Почтовый адрес:</span>
					                <input name="flamix_theme_options[post_address]" value="<?php echo $options['post_address'];?>" />
					            </label>
							</li>
							<li>
								<label>
					            	<span>Наш адрес:</span>
					                <textarea name="flamix_theme_options[our_address]" ><?php echo $options['our_address'];?></textarea>
					            </label>
							</li>
							
						</ul>
					</div>
					<div class="tab-content">
						<ul class="b-social">
							<li class="vk">
								<span>
									ВКонтакте
								</span>
								<input type="text" name="flamix_theme_options[vk]" id="flamix_theme_options[vk]" value="<?php echo $options['vk'];?>" />
							</li>
							<li class="facebook" >
								<span>
									Facebook						
								</span>
								<input type="text" name="flamix_theme_options[facebook]" id="flamix_theme_options[facebook]" value="<?php echo $options['facebook'];?>" />
							</li>
							<li class="instagram">
								<span>
									Instagram
								</span>
								<input type="text" name="flamix_theme_options[instagram]" id="flamix_theme_options[instagram]" value="<?php echo $options['instagram'];?>" />
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="bottom">
				<button type="submit" data-text="Применить" ><span>Применить</span></button>
			</div>
		</div>
		<div class="tab-lang kz">
			<div class="wrapp-content">
				<div class="left-block">
					<ul>
						<li class="active">Байланыстар</li> 
						<li >Әлеуметтік желілер</li>
					</ul>
				</div>
				<div class="right-block">
					
					<div class="tab-content active">
						<ul class="b-contact">
							<li>
								<label>
					            	<span>Қосымша ақпарат алу үшін қоңырау:</span>
					            	<input type="tel" name="flamix_theme_options[phone_kz]" value="<?php echo $options['phone_kz'];?>" />
					            </label>
							</li>
							<li>
								<label>
					            	<span>Караоке Systems:</span>
					                <input name="flamix_theme_options[karaoke_tel_kz]" value="<?php echo $options['karaoke_tel_kz'];?>" />
					            </label>
							</li>
							<li>
								<label>
					            	<span>Дыбыс жабдық.:</span>
					                <input name="flamix_theme_options[sound_tel_kz]" value="<?php echo $options['sound_tel_kz'];?>" />
					            </label>
							</li>
							<li>
								<label>
					            	<span>Жеңіл жабдық.:</span>
					                <input name="flamix_theme_options[light_tel_kz]" value="<?php echo $options['light_tel_kz'];?>" />
					            </label>
							</li>
							<li>
								<label>
					            	<span>Пошталық мекенжай:</span>
					                <input name="flamix_theme_options[post_address_kz]" value="<?php echo $options['post_address_kz'];?>" />
					            </label>
							</li>
							<li>
								<label>
					            	<span>Біздің мекен жай:</span>
					                <textarea name="flamix_theme_options[our_address_kz]" ><?php echo $options['our_address_kz'];?></textarea>
					            </label>
							</li>
							
						</ul>
					</div>
					<div class="tab-content">
						<ul class="b-social">
							<li class="vk">
								<span>
									ВКонтакте
								</span>
								<input type="text" name="flamix_theme_options[vk_kz]" id="flamix_theme_options[vk_kz]" value="<?php echo $options['vk_kz'];?>" />
							</li>
							<li class="facebook" >
								<span>
									Facebook						
								</span>
								<input type="text" name="flamix_theme_options[facebook_kz]" id="flamix_theme_options[facebook_kz]" value="<?php echo $options['facebook_kz'];?>" />
							</li>
							<li class="instagram">
								<span>
									Instagram
								</span>
								<input type="text" name="flamix_theme_options[instagram_kz]" id="flamix_theme_options[instagram_kz]" value="<?php echo $options['instagram_kz'];?>" />
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="bottom">
				<button type="submit" data-text="Қолдану" ><span>Қолдану</span></button>
			</div>
		</div>
		</form>
	 </div>
	<?php
	}
	/**
	 * Loads the image management javascript
	 */
	public static function prfx_image_enqueue() {
	    global $typenow;
	        wp_enqueue_media();
	 
	        // Registers and enqueues the required javascript.

	        wp_register_script( 'meta-box-image', get_template_directory_uri() . '/inc/meta-box-image.js', false, '1.0.0' );
	        wp_localize_script( 'meta-box-image', 'meta_image',
	            array(
	                'title' => __( 'Choose or Upload an Image', 'prfx-textdomain' ),
	                'button' => __( 'Use this image', 'prfx-textdomain' ),
	            )
	        );
	        wp_enqueue_script( 'meta-box-image' );
	}
}


 

  

